import axios from 'axios';

const KEY = 'AIzaSyDqjYaSWqF7ltS4M7g3S1SZYUJKEeZDNIw';

export default axios.create( {
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params: {
    part: 'snippet',
    maxResults: '5',
    key: KEY
  }
});
