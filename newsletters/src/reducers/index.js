import { combineReducers } from 'redux';
import newslettersReducer from './newslettersReducer';
import categoriesReducer from './categoriesReducer';

export default combineReducers({
  newsletters: newslettersReducer,
  categories: categoriesReducer
});
