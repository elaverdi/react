export default (state = [], action) => {
  switch(action.type) {
    case 'FETCH_NEWSLETTERS':
      return action.payload;
    default:
      return state;
  }
}
