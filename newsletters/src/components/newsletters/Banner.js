import React from 'react';
import { connect } from 'react-redux';

class Banner extends React.Component {
  componentDidMount() {
  }

  render() {
    return (
      <section className="hero" style={{backgroundImage: 'url(https://www.trbimg.com/img-5a04beda/turbine/la-lat-newsletter-feature-20171109)'}}>
        <div className="grid-container fluid">
          <div className="grid-x">
            <div className="cell small-12">
              <h1>Los Angeles Times Newsletter</h1>
              <p>The stories that matter the most. Delivered free to your email inbox.</p>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default connect()(Banner);
