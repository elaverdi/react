import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { fetchCategories, filterCategories } from '../../actions';

class Categories extends React.Component {
  componentDidMount() {
    this.props.fetchCategories();
  }
  onCategoryClick(name) {
    this.props.filterCategories(name);
  }
  renderList() {
    return this.props.categories.map( item => {
      return (
        <li  className={(item.active ? 'active' : '')} onClick={ () => this.onCategoryClick(item.name)}  key={_.uniqueId()}>{item.name}</li>
      );
    });
  }

  render() {
    return (
      <div>
      <ul className="vertical menu">
        <li onClick={ () => this.onCategoryClick('all')}>View All</li>
        {this.renderList()}
      </ul>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return { categories: state.categories, filterCategories: state.filterCategories };
}

export default connect(mapStateToProps, { fetchCategories, filterCategories })(Categories);
