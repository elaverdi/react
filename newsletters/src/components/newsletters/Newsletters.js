import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { fetchNewsletters, subscribe, fetchCategories } from '../../actions';

class Newsletters extends React.Component {

  componentDidMount() {
  }

  state = { email: '', newsletterId: '' };

  onInputChange = (event) => {
    this.setState({ email: event.target.value});
  }

  onInputClick = (event) => {
    if(this.state.email !== '') {
      event.target.value = this.state.email;
    }
    event.target.nextSibling.style.display = 'flex';
  }

  onFormSubmit = (event) => {
    event.preventDefault();
    let response = this.props.subscribe(this.state.email, event.target.dataset.newsletterid, event.target.dataset.newslettername);

    response.then(function(subscribed) {
      if(subscribed) {
        alert('Thank you for subscribing.')
      }
      else {
        alert('Please input a valid email address.')
      }
    });
  }

  renderNewsletters(item) {
    if(!this.props.newsletters)
      return
    else {
      return _.filter(this.props.newsletters, { 'category.name': item}).map( item => {
        return (
          <div className="cell" key={item['newsletter.id']}>
            <div className="card">
              <div className="card-category">{item['category.name']}</div>
              <div className="cover" style={{ backgroundImage: "url(" + item['categories.landing_page.image_url'] + ")"}}></div>
              <div className="card-section">
                <label>{item['categories.landing_page.frequency.name']}</label>
                <h4>{item['newsletter.name']}</h4>
                <p>{item['categories.landing_page.description']}.</p>
                <form onSubmit={this.onFormSubmit} data-newsletterid={item['newsletter.id']} data-newslettername={item['newsletter.name']}>
                  <div className="input-group active">
                    <input name="newsletterd-id" className="input-group-field" placeholder="Email Address" type="text" onClick={this.onInputClick} onChange={this.onInputChange} />
                    <div className="input-group-button">
                      <button className="button" type="submit">&gt;</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        );
      });
    }
  }

  renderCategories() {
    if(_.filter(this.props.categories, {'active': true}).length === 0) {
      return this.props.categories.map( item => {
        return (
          <div key={_.uniqueId()}>
            <h2>{item.name}</h2>
            <div className="grid-x grid-padding-x small-up-1 medium-up-2 large-up-2 xlarge-up-3">
              {this.renderNewsletters(item.name)}
            </div>
          </div>
        );
      });
    }
    else {
      return this.props.categories.map( item => {
        if(item.active) {
          return (
            <div key={_.uniqueId()}>
              <h2>{item.name}</h2>
              <div className="grid-x grid-padding-x small-up-1 medium-up-2 large-up-2 xlarge-up-3">
                {this.renderNewsletters(item.name)}
              </div>
            </div>
          );
        }
        else {
          return '';
        }
      });
    }
  }

  render() {
    return (
      <div>
        {this.renderCategories()}
      </div>
    );
  }

}
const mapStateToProps = (state) => {
  return { newsletters: state.newsletters, subscribe: state.subscribe, categories: state.categories };
}

export default connect(mapStateToProps, {fetchNewsletters, subscribe, fetchCategories})(Newsletters);
