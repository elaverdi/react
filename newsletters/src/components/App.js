import React from 'react';
import Banner from './newsletters/Banner';
import Categories from './newsletters/Categories';
import Newsletters from './newsletters/Newsletters';

const App = () => {
  return (
    <div>
      <div className="app-header"><Banner /></div>
      <div className="app-body grid-container">
        <div className="grid-x grid-margin-x">
          <div className="cell small-12 large-2"><Categories /></div>
          <div className="cell small-12 large-10"><Newsletters /></div>
        </div>
      </div>
    </div>
  );
};

export default App;
