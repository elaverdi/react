import _ from 'lodash';
import nlr from '../apis/nlr';

export const fetchNewsletters = () => async dispatch => {
  const response = await nlr.get('/stage-v3/newsletters/affiliate/balnews?categorize=1');
  dispatch({ type: 'FETCH_NEWSLETTERS', payload: response.data });
};

export const fetchCategories = () => async (dispatch, getState) => {
  await dispatch(fetchNewsletters());
  const response = [];
  _.forEach(_.uniq(_.map(getState().newsletters, 'category.name')) , function(value) {
    response.push({'name': value, 'active': false});
  });
  dispatch({ type: 'FETCH_CATEGORIES', payload: _.orderBy(response, ['name']) });
};

export const filterCategories = (name) => async (dispatch, getState) => {
  const categories = getState().categories;
  console.log('before', categories);
  const response = [];
  if(name === 'all') {
    _.forEach(categories, function(obj, key) {
      response.push({'name': obj.name, 'active': false});
    });
  }
  // else if(_.filter(categories, {'active': false}).length === categories.length) {
  //   _.forEach(categories, function(obj, key) {
  //     if(obj.name === name) {
  //       response.push({'name': obj.name, 'active': !obj.active});
  //     }
  //     else {
  //       response.push({'name': obj.name, 'active': false});
  //     }
  //   });
  // }
  else {
    _.forEach(categories, function(obj, key) {
      if(obj.name === name) {
        response.push({'name': obj.name, 'active': !obj.active});
      }
      else {
        response.push({'name': obj.name, 'active': obj.active});
      }
    });
  }
  console.log('after', response);
  dispatch({ type: 'FETCH_CATEGORIES', payload: response });
};

export const subscribe = (email,id,name) => async (dispatch, getState) => {
  if(validEmail(email)) {
    console.log(`Subscribing ${email} to ${name} - ${id}.`);
    return true;
  }
  else {
    return false
  }
}

export const validEmail = (email) => {
  const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return pattern.test(String(email).toLowerCase());
};
