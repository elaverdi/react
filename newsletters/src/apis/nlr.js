import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.nlr.tronc.com'
});
