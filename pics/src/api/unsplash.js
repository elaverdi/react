import axios from 'axios';

export default axios.create({
  baseURL: 'https://api.unsplash.com',
  headers: { Authorization: 'Client-ID 4b1fa5f1105715d133808c3a9251a7a6cad1776ac5a54b2a0b751718298a80bf' }
});
